import { Component } from '@angular/core';
import { Platform, Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';
import { UserService } from '../providers/user.service';


@Component({
  templateUrl: 'app.html',
  providers: [UserService]
})
export class MyApp {
  rootPage:any;

  constructor(
    public platform: Platform, 
    public statusBar: StatusBar, 
    public splashScreen: SplashScreen,
    public events: Events,
    private userService: UserService
  ) {
    
    this.userService.hasLoggedIn()
      .then((hasLoggedIn) => {
        if(hasLoggedIn) {
          this.rootPage = TabsPage;
        } else {
          this.rootPage = LoginPage;
        }
        this.platformReady();
    });

  }

  platformReady() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    })
  }
}
