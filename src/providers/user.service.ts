import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Events } from 'ionic-angular';
import { NativeStorage } from '@ionic-native/native-storage';
import { Observable } from 'rxjs/Observable';
import "rxjs/add/operator/map";
import "rxjs/add/operator/do";
import "rxjs/add/operator/catch";
import "rxjs/add/observable/throw";

import { User } from '../interfaces/user';
import { Config } from '../config';
/*
  Generated class for the UserDataProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserService {

  HAS_LOGGED_IN = 'hasLoggedIn';

  constructor(private http: HttpClient, private nativeStorage: NativeStorage) {}

  login(user: User) {
    let headers = new HttpHeaders();
    headers.append("Content-Type", "application/json");
  
    return this.http.post(
      Config.apiUrl + "ustUsers/login",
      JSON.stringify({
        username: user.email,
        password: user.password,
      }),
      { 
        headers: headers,
        responseType: 'json'
      }
    )
    .do(data => {
      this.nativeStorage.setItem('userInfo', {
        id: data['userId'],
        token: data['id']
      });
      this.nativeStorage.setItem(this.HAS_LOGGED_IN, true);
    })
    .catch(this.handleErrors);
  } 

  hasLoggedIn(): Promise<boolean> {
    return this.nativeStorage.getItem(this.HAS_LOGGED_IN)
      .then((value) => { return value === true });
  }

  handleErrors(error: Response) {
    console.log(JSON.stringify(error.json()));
    return Observable.throw(error);
  }

}
