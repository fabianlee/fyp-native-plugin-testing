import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { TabsPage } from '../tabs/tabs';
import { UserService } from '../../providers/user.service';
import { User } from '../../interfaces/user';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
  providers: [UserService]
})
export class LoginPage {

  public backgroundImage = "../../assets/imgs/login-bg.jpg";
  submitted = false;
  user: User;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private userService: UserService
  ) {
    this.user = new User();
    this.user.email = '';
    this.user.password = '';
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  login(form: NgForm) {
    this.submitted = true;
    if(form.valid) {
      this.userService.login(this.user);
      this.navCtrl.push(TabsPage);
    }
  }

}
